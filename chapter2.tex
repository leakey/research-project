\chapter{Single particle Dirac equation}\label{ch.2}
As a consequence of the Klein-Gordon theory's failure \citep{Armin} to provide an amicable interpretation of the negative energy solutions and to explain the lack of positive definite probability density for the wave-function, Paul Dirac (1928) came up with a relativistic equation that could be used to describe the spin $\frac{1}{2}$ particles and provide a better explanation to the constraints experienced by relativistic Klein- Gordon equation to a greater extent.

In this chapter, we discuss the derivation of the relativistic Dirac equation from the first order time dependent Schrödinger equation and express it in both canonical and Lorentz covariant form. What is more, we solve the equation explicitly and discuss the significance of the spinors in relation to the equation. 

\section{The Dirac equation}\label{2.1}

The one-particle interpretation of the Klein-Gordon equation was not seriously considered until Dirac introduced the relativistic quantum mechanics for the spin $\frac{1}{2}$ particles \citep{Armin}.
In order to understand this relativistic consideration for the spin $\frac{1}{2}$ particles, the following principles must hold:
\begin{itemize}
\item[$\bullet$] We must obtain the non-relativistic quantum mechanics in the appropriate limit.
\item[$\bullet$] Limitation of one particle interpretation to the small interaction energy in comparison to the rest energy and position uncertainty of the wave function \citep{Keith}.
\end{itemize}

However, the Dirac equation that we will discuss here also experienced a problem that is universal to every relativistic wave equation, that is, it also gives the negative energy solutions whose physical meaning and/or interpretation is still open for further probe.
In this section, we will notice that solutions to Dirac equation exhibit an inner degree of freedom that can be attributed to the spin of the particle $s=\frac{1}{2}$.

\subsection{Canonical form}~

Generally, the relativistic energy-momentum relation for the free particle is given by:
\begin{equation}
E^2=\mathbf{p}^2c^2+ m^2c^4.
\end{equation}
We assign the natural units
\[\hbar=c=1,\]
so that we have
\begin{align}
E^2=\mathbf{p}^2+ m^2.
\end{align}\label{eqn.2.1.2}
From the correspondence principle, the operator replacement for time and position can be done as follows \citep{Kresimir}.
\begin{align*}
E\rightarrow i\partial_t , ~~~ \mathbf{p}\to -i\nabla.
\end{align*}
If we implement these operators into equation (\ref{eqn.2.1.2}), we get the Klein-Gordon equation which is an equation for a  scalar field, expressed as
\[\left[\square +m^2\right]\psi=0.\]
This is the equation that experienced the above mentioned constraints.
Dirac started by considering a relativistic generalization  of the time dependent Schrödinger equation for a free particle:
\begin{align}
i\partial_t\psi(x)=H \psi(x),\label{eqns 2.1.3}
\end{align}
where the energy operator $H$ is hermitian and $x=x^\mu=\left(\begin{array}{c}
t\\
\mathbf{x}
\end{array}\right)$.

From equation \eqref{eqns 2.1.3}, he made the following hypotheses:
\begin{enumerate}
\item The equation is first order in time and space.\label{1.0}
\item The equation should give an energy-momentum relation in operator form.\label{2.0}
\item The probability density $\rho=\psi^*\psi$ and the four-current vector $j^\mu$ are conserved ($\partial_\mu j^\mu=0$) in time and space and related by the continuity equation.
\end{enumerate}
%\[\partial_\mu j^\mu=0\]

From \ref{1.0} and \ref{2.0}, the following \textbf{ansatz} is sufficient
\begin{align}
H =\bold{p}\cdot \boldsymbol{\alpha}  +\beta m \label{eqn 2.1.4}
\end{align}
where $m$ is the rest mass of the particle.
This Hamiltonian is equivalent to the relativistic energy hence it can also be secondarily expressed as
\begin{align*}
H^2 =\bold{p}^2  + m^2
\end{align*}
or
\begin{align*}
-\frac{\partial^2}{\partial t^2} \psi(x) =(\bold{p}^2  + m^2)\psi(x).
\end{align*}
This is the  Klein-Gordon equation.
From this equation, we can deduce that $\alpha_i$ and $\beta$ have a certain algebraic form which we have to find as there are no mixed terms in $\boldsymbol{\alpha}\cdot\bold{p}$ and $\beta$. According to \cite{Armin}, we can proceed in the following manner to determine the nature of these unknowns.

From equation (\ref{eqns 2.1.3}) above, we have
\[i \partial_t \psi= \left(\frac{1}{i}\sum_i \alpha_i \partial_i+ \beta  m\right)\psi.\]
Multiplying both sides by $i\partial_t$,
\[-\frac{\partial^2\psi}{\partial t^2} =i\partial_t\left(\frac{1}{i}\sum_i \alpha_i \partial_i\psi+ \beta  m\psi\right)\]
\[ =\frac{1}{i}\sum_j \alpha_j\partial_j\left(\frac{1}{i}\sum_i \alpha_i \partial_i\psi+ \beta  m\psi\right)+\beta m\left(\frac{1}{i}\sum_i \alpha_i \partial_i\psi+ \beta  m\psi\right),\]
hence
\[-\frac{\partial^2\psi}{\partial t^2}=-\sum_{i,j}\frac{\alpha_i\alpha_j+\alpha_j\alpha_i}{2}\partial_i\partial_j\psi+\frac{m}{i}\sum_i\left(\alpha_i\beta+\beta\alpha_i\right)\partial_i\psi+\beta^2m^2\psi.\]
Since $\partial_i\partial_j=\partial_j\partial_i $, we clearly see that equation (\ref{eqn 2.1.4}) can only be valid if $\alpha_i$ and $\beta$ are matrices for which the following conditions hold
\begin{align}
\alpha_i^2=\beta^2=1,\, \{\alpha_i,\beta\}=0 ,\,\{\alpha_i,\alpha_j\}=2\delta_{ij}\label{eqn 2.1.5}
\end{align}
and $\alpha_i=\alpha_i^\dagger,~\beta=\beta^\dagger$ since $H$ is to be hermitian. Then the eigenvalues of the operator matrices must be $\pm 1$ with vanishing \textit{traces}. The dimension of each matrix is even-numbered with the least even number $n=2$ leading to the three known Pauli matrices but we need at least four such matrices, hence, we will work with $n=4$.
From the conditions in equation (\ref{eqn 2.1.5}) above, we can get the explicit Dirac representation of the matrices $\alpha_i$ and $\beta$ \citep{Keith}
\[\alpha_i=\left(\begin{array}{cc}
0&\sigma_i\\
\sigma_i&0
\end{array}\right),~~ \beta=\left(\begin{array}{cc}
1&0\\
0&-1
\end{array}\right)\]
where $\sigma_i$ are the Pauli matrices
\[\sigma_1=\left(\begin{array}{cc}
0&1\\
1&0
\end{array}\right),~~~\sigma_2=\left(\begin{array}{cc}
0&-i\\
i&0
\end{array}\right),~~~ \sigma_3=\left(\begin{array}{cc}
1&0\\
0&-1
\end{array}\right),\]
with the identity
\begin{equation}
\sigma_i\sigma_j=\delta_{ij}+i\varepsilon_{ijk}\sigma_k .\label{id}
\end{equation}
If we take the least appropriate dimension, $n=4$, we get the free particle Schrödinger equation \eqref{eqn2.1.13} transformed into:
\begin{align}
i\partial_t\psi_i(x)=\sum_{j=1}^4 \underbrace{\bigg((\boldsymbol{\alpha}\cdot\bold{p})_{ij}+\beta_{ij}m\bigg)}_H\psi_j(x), ~\text{with}~~ i=1,2,\cdots 4.\label{eqn 2.1.6}
\end{align}
From this expression, we note that the bispinor, $\psi(x)$ must now be a 4$\times$1 matrix. This is the free particle Dirac equation in canonical/ Hamilton form. The Hamiltonian $H$ is hermitian thus we can achieve a positive definite probability density which obeys the continuity equation.
\[\partial_t \rho +\nabla\cdot\bold{j}=0\]
where the current density and the probability density are given by $\bold{j}=\psi^\dagger\boldsymbol{\alpha}\psi $ and $\rho=\psi^\dagger\psi $  respectively. 
\begin{proof}~
We now prove that $\partial_t \rho +\nabla\cdot\bold{j}=0$ for  $\bold{j}=\psi^\dagger\boldsymbol{\alpha}\psi $ and $\rho=\psi^\dagger\psi $ by proceeding as follows:

From equation \eqref{eqn 2.1.6}, we can alternatively write the canonical form of Dirac equation as follows
\[i\partial_t\psi=\big(\boldsymbol{\alpha}\cdot\bold{p}+\beta m\big)\psi=H\psi.\]
We find the probability current for this wave function by multiplying the canonical equation by $\psi^\dagger$ from the left and subtracting with its adjoint multiplied by $\psi$ from the right \citep{Karolos}
\[\psi^\dagger i\partial_t \psi=\psi^\dagger\big(-i\boldsymbol{\alpha}\cdot\nabla+\beta m\big)\psi,\]
\[-\big(i\partial_t\psi^\dagger\big)\psi=\big(i\nabla\psi^\dagger\cdot \boldsymbol{\alpha}+m\psi^\dagger\beta\big)\psi.\]
Subtracting the two equations results in
\[i\partial_t\psi^\dagger\psi+i\nabla\big(\psi^\dagger\boldsymbol{\alpha\psi}\big)=0\]
and proves the above statement.
\end{proof}

%For a minimally coupled electromagnetic field (discussed in section \ref{sec2.3}), 
%\[H=\bold{\alpha}\cdot\left(\bold{p}-e\bold{A}\right)+e\phi+\beta m\]
%where $\bold{A}=(\phi,\bold{A})$ is the coupling vector potential.
\subsection{Lorentz covariance form}~

We now show that the Dirac equation has the same form in all inertial systems, based on the earlier stated principles i.e the symmetry between $ct=x^0$ and $x^i$ must be maintained in accordance with the relativity principle.

We therefore introduce the $\gamma$-matrices that are defined in Clifford algebra.
\[\{\gamma^\mu,\gamma^\nu\}=2g^{\mu\nu},~~~ (\gamma^\mu)^2=g^{\mu\mu}\]
%\[g^{\mu\nu}=\left[\begin{array}{cccc}
%%1&0&0&0\\
%%0&-1&0&0\\
%%0&0&-1&0\\
%%0&0&0&-1
%%\end{array}\right]\]
\begin{align}
\beta=\gamma^0=\left[\begin{array}{cc}
1&0\\
0&-1
\end{array}\right],~~~ \gamma^i=\left[\begin{array}{cc}
0&\sigma_i\\
-\sigma_i&0
\end{array}\right]=\beta\alpha_i . \label{2.1.8}
\end{align}
Now, since $\alpha_i$ and $\beta$ are hermitian, 
\[\gamma^{\mu\dagger}=g^{\mu\mu}\gamma^\mu\Longleftrightarrow \gamma^{\mu\dagger}=\gamma^0\gamma^\mu\gamma^0\] \citep{Schwartz}.

The interaction with the electromagnetic field is incorporated by minimal substitution
\[p_{\mu}\to p_{\mu}-eA_\mu\]
where $A^{\mu}$ is the vector field while
\begin{align}
\bold{E}=-\nabla \phi -\partial_t\bold{A}~~ \text{and} ~~ \bold{B}= \nabla \times \bold{A}
\label{fld}
\end{align}
in the normal relativistic representation. We can now rewrite the canonical equation (for a minimally coupled electromagnetic field) as
\begin{align}
\left[\gamma^\mu\big(p_\mu-eA_\mu\big)-m\right]\psi=0
\end{align}
\[\text{where}~ A_\mu=A_\mu(x)~ \text{and} ~ \psi=\psi(x)\]

or,
\begin{align*}
(i\gamma^\mu\partial_\mu-m)\psi=e\slashed A \psi \Longrightarrow (i\slashed{\partial} -m)\psi=e\slashed A \psi
\end{align*}
or, for a completely free field (without any electromagnetic coupling),
\begin{align}
(i\slashed{\partial} -m)\psi=0.
\end{align}
In order to have a form invariant Dirac equation, we will consider two inertial frames $T_1$ and $T_2$ with observers $O_1$ and $O_2$ respectively \citep{Kresimir} such that, if we have a wave function $\psi(x^\mu)$ describing a particle in $T_1$ then we can determine a corresponding $\psi'(x'^\mu)$ in $T_2$ that describes the same particle. 
Here, if $\Lambda$ is the transformation of the coordinates, then $x'^\nu=\Lambda^{\nu}_\mu x^\mu$ while $\psi(x^\mu)$ and $\psi'(x'^\mu)$ are solutions of Dirac equation in $T_1$ and $T_2$ respectively;
\begin{align}
\bigg(i\gamma^\mu\frac{\partial}{\partial x^\mu}-m\bigg)\psi(x^\mu)=0\label{eqn2.1.9}
\end{align}
and
\begin{align}
\bigg(i\gamma^\nu\frac{\partial}{\partial x'^\nu}-m\bigg)\psi'(x'^\nu)=0.\label{eqn 2.1.10}
\end{align}
If the Dirac equation is Lorentz covariant, then it implies  that the $\gamma$-matrices are the same in $T_1$ and $T_2$ and therefore we proceed by finding a transformation matrix $M$ such that
\begin{align}
M\psi(x)=\psi'(\Lambda x).\label{eqn 2.1.11}
\end{align}
\begin{proof}
Suppose we apply the condition in equation (\ref{eqn 2.1.11})  to equation (\ref{eqn2.1.9}), we obtain the following.
\begin{align*}
iM\gamma^\mu M^{-1}\frac{\partial}{\partial x^\mu} M\psi(x^\mu)-mM\psi(x^\mu)=0
\end{align*}
implying that
\begin{align}
iM\gamma^\mu M^{-1}\frac{\partial}{\partial x^\mu}\psi'(x'^\nu)-m\psi'(x'^\nu)=0.
\end{align}
Applying the chain rule
\[\frac{\partial}{\partial x^\mu}=\frac{\partial}{\partial x'^\nu}\frac{\partial x'^\nu}{\partial x^\mu}=\Lambda_{\mu}^\nu\frac{\partial}{\partial x'^\nu}.\]
We therefore have
\begin{align*}
iM\gamma^\mu M^{-1}\Lambda_{\mu}^\nu\frac{\partial}{\partial x'^\nu} \psi'(x'^\nu)-m\psi'(x'^\nu)=0,
\end{align*}
or 
\begin{align}
(iM\gamma^\mu M^{-1}\Lambda_{\mu}^\nu\frac{\partial}{\partial x'^\nu}-m)\psi'(x'^\nu)=0.\label{eqn2.1.13}
\end{align}

If we compare equations \eqref{eqn2.1.13} and \eqref{eqn 2.1.10}, we notice that
\[\gamma^\nu=M\gamma^\mu M^{-1}\Lambda_{\mu}^\nu,\]
or more explicitly,
\begin{align}
M(\Lambda)\gamma^\mu M^{-1}(\Lambda)=(\Lambda^{-1})_{\mu}^\nu \gamma^\nu. \label{2.1.14}
\end{align}
\end{proof}
%The following theorem which we will not prove here, describes the existence of $M$.
%\begin{thm}
%Fundamental Pauli theorem, \cite{Karolos}- If two matrices $\gamma^\mu$ and $\gamma^\nu$ obeys the commutation relation $\{ \gamma^\mu,\gamma^\nu \}=2g^{\mu\nu}$, then there exists a non-singular matrix $M$ such that the equation  $\gamma^\nu M=M\gamma^\mu $ is satisfied.
%\end{thm}
%This theorem guarantees the existence of $M$ which can be determined upto a factor provided that the condition in equation \eqref{2.1.14} is satisfied.
We have shown that the Dirac equation is form invariant under Lorentz transform and therefore we can conclude that it is a relativistic generalisation of Schrödinger equation. The matrix $M$ can be obtained from $\Lambda_\mu^\nu$ by iterating the infinitesimal Lorentz transformations.

\section{Explicit solution}\label{2.2}

Let us now consider the Hamiltonian in the free Dirac equation \eqref{eqn 2.1.6} above
\begin{align}
H= \boldsymbol{\alpha}\cdot\bold{p}+\beta m,~~~ i\partial_t\psi =H\psi \label{eqn 2.2.1}
.\end{align}

We will have an \textbf{ansatz} for the solution of the equation, expressed as
\begin{align*}
\psi(x)=\left(\begin{array}{c}
\varphi_0\\
\chi_0
\end{array}\right)e^{i(\bold{p}\cdot\bold{x}-Et)}.
\end{align*}
\citep{Armin}

%Then, we have:
%\begin{align*}
%\psi(x)=\left(\begin{array}{c}
%\varphi_0\\
%\chi_0
%\end{array}\right)e^{i(\vec{p}.\vec{x}-Et)}
%\end{align*}
In this case, $\psi_0$ and
$\chi_0$ are the component spinors.
If we substitute $\psi(x)$ into equation \eqref{eqn 2.2.1}, we  have
\begin{align}
  \begin{array}{l l}
    (E-m)\varphi_0-\boldsymbol{\sigma}\cdot\bold{p}\chi_0=0 ,\\
    -\boldsymbol{\sigma}\cdot\bold{p}\varphi_0+(E+m)\chi_0=0 .\label{eqn 2.2.2}
  \end{array} 
\end{align}
Non-trivial solutions exists for this pair of equations only if the coefficient determinants vanishes hence
\begin{align*}
\left|\begin{array}{cc}
E-m&-\boldsymbol{\sigma}\bold{p}\\
-\boldsymbol{\sigma}\bold{p} & E+m
\end{array}\right|=E^2-m^2-(\boldsymbol{\sigma}\cdot\bold{p})(\boldsymbol{\sigma}\cdot\bold{p})=0.
\end{align*}
We now use the identity $(\boldsymbol{\sigma}\cdot\bold{p})^2=\bold{p}^2$ to obtain%$(\vec{\sigma}.\vec{P})(\vec{\sigma}.\vec{Q})=P.Q+i\vec{\sigma}.(\vec{P}\times\vec{Q})} and $, 

\begin{align}
E=\pm \sqrt{\bold{p}^2+m^2}
\end{align}

  %\begin{array}{l l}
%\\
%~\\
%E_-=-\sqrt{\vec{p}^2+m^2}=-E
 %\end{array} 
as expected. We now substitute these into equation \eqref{eqn 2.2.2} which yields
\begin{align}
  \begin{array}{l l}
    \psi^+(x)=\left(\begin{array}{c}
    \chi^{(\xi)}\\
    \frac{\boldsymbol{\sigma}\cdot\bold{p}}{p+m}\chi^{(\xi)}
    \end{array} \right)e^{-i(E-\bold{p}\cdot \bold{x})},\\
    ~\\
    \psi^-(x)=\left(\begin{array}{c}
    \frac{-\boldsymbol{\sigma}\cdot\bold{p}}{p+m}\chi^{(\xi)}\\
    \chi^{(\xi)}
\end{array} \right)e^{i(E+\bold{p}\cdot\bold{x})},
  \end{array} 
\end{align}

where, $\xi=(1,2)$, $e^{-i(E-\bold{p}.\bold{x})}\sim \psi_{\bold{p}}^{(\xi)}(x)$ and $e^{i(E+\bold{p}\cdot\bold{x})} \sim  \psi_{-\bold{p}}^{(\xi)}(x)$.

Since $\chi^{(\xi)}$ are generally constant component spinors up to normalization, we can reformulate our \textbf{ansatz} to read
\[\psi(x)=u(\bold{p})e^{-ipx}\]
which, when we include into the Dirac equation, gives the momentum space Dirac equation \citep{Kresimir}.
\[(\slashed p-m)u(\bold{p})=0.\]
%Since $E=pc$(from Einstein's theory) and $c=1$, we have:
We now have
\begin{align}
  \begin{array}{l l}
    u_+(\bold{p},\xi)=N\left(\begin{array}{c}
    \chi^{(\xi)}\\
    \frac{\boldsymbol{\sigma}\cdot\bold{p}}{E+m}\chi^{(\xi)}
    \end{array} \right), \\
    ~\\
    u_-(\bold{p},\xi)=-N\left(\begin{array}{c}
    \frac{-\boldsymbol{\sigma}\cdot\bold{p}}{E+m}\chi^{(\xi)}\\
    \chi^{(\xi)}
\end{array} \right).
  \end{array} 
\end{align}
where $N=\sqrt{\frac{E+m}{2E}}$ is the normalization constant which can be calculated using the identity $u^\dagger u=1$ $\chi^{(1)}=\left(\begin{array}{c}
1\\
0
\end{array}\right)$ and $\chi^{(2)}=\left(\begin{array}{c}
0\\
1
\end{array}\right)$ are the spinors.

\textbf{Note:} For our case, we will only use the positive energy for the free electron in a momentum space, i.e.
\begin{align}
    u_+(\bold{p},\xi)=N\left(\begin{array}{c}
    \chi^{(\xi)}\\
    \frac{\boldsymbol{\sigma}\cdot\bold{p}}{E+m}\chi^{(\xi)}
    \end{array} \right). \label{2.2.6}
\end{align}

\section{Coupling of magnetic field and Pauli Hamiltonian}\label{sec2.3}

A charged electron $e$ in an electromagnetic field experiences a Lorentz force expressed as
\[\bold{F}=e\big(\bold{E}+\bold{v}\times \bold{B}\big),\]
where $\bold{v}$ is the velocity of the electron. 
If a vector potential $\bold{A}(x)$ and a Coulomb potential $\phi(x)$ are the corresponding potentials to electric and magnetic field respectively, then the conditions in \ref{fld} holds.
%\[\bold{E}=-\nabla\phi-\partial_t \bold{A}~ \rm and~ \bold{B}=\nabla\times \bold{A}\].
Classically, it can be proved \citep{Walter},  (pg.213-214) that this motion is described by a Hamiltonian function given as
\begin{align}
H=\frac{1}{2m}\big(\bold{p}-e\bold{A}\big)^2+e\phi. \label{H}
\end{align}
The terms  $\bold{p}-e\bold{A}$ and $e\phi$ are the minimum coupling while the canonical momentum $\bold{p}$ can be expressed as,
\[\bold{p}=m\bold{v}+e\bold{A}\]
and is determined only by the vector potential. 
Using the condition that $p\to -i\nabla$ in a quantum field, we can transform the classical Hamiltonian function into an operator
\begin{align}
\hat{H}=\frac{1}{2m}\big(-i\nabla-e\bold{A}\big)^2+e\phi.
\end{align}
Noting that gradient and vector potentials do not commute, we can expand the Hamiltonian \cite{Walter} to obtain
\[\hat{H}=-\frac{1}{2m}\nabla^2+\frac{ie}{m}(\bold{A}\cdot\nabla)+\frac{ie}{2m}(\nabla\cdot\bold{A})+\frac{e^2}{2m}\bold{A}^2+e\phi.\]
Since $\nabla\cdot\bold{A}=0$ ($\bold{A}$ and $\phi$ are not unique but Gauge dependent-\textit{studied in Gauge theory not discussed here}).
Rearranging the terms and using the momentum operator $\hat{p}$, we have
\[\hat{H}=\frac{\hat{p}^2}{2m}+e\phi-\frac{e}{m}\bold{A}\cdot\hat{p}+\frac{e^2}{2m}\bold{A}^2,\]
or
\begin{align}
\hat{H}=\hat{H_0}-\frac{e}{m}\bold{A}\cdot\hat{p}+\frac{e^2}{2m}\bold{A}^2.
\end{align}%=\]
The operator $\hat{H_0}$ represents the motion in an uncoupled field while $(\bold{A}\cdot\hat{p})$ represents the motion in a coupled field. Moreover, the third term only depends on the vector potential $\bold{A}$ and therefore can be dropped when the field is weak \citep{Mano}. Therefore, if $\textbf{A}$ describes a plane electromagnetic wave then the state of the electromagnetic field can be given as a solution of the Schrödinger equation:
\[\underbrace{\frac{1}{2m}\big(\bold{p}-e\bold{A}\big)^2+e\phi)}_{H (\ref{H})} \psi=i\partial_t \psi.\]
\subsection{The Pauli Hamiltonian}~\label{sec2.3.1}

We now derive the non-relativistic quantum dynamical equation of a spin $\frac{1}{2}$ charged particle of charge $e$ in an external vector potential $\bold{A}$ and a scalar potential $\phi$ such that:
\[E\to E-e\phi,~~\bold p\to \bold p-e\bold A,~~\psi=\left(\begin{array}{c}
\varphi_0\\
\chi_0
\end{array}\right).\]
We can now write equation \eqref{eqn 2.2.2} in terms of  the component spinors
\[\gamma^0(E-e\phi)\left(\begin{array}{c}
\varphi_0\\
\chi_0
\end{array}\right)-\gamma (\bold p-e\bold A)\left(\begin{array}{c}
\varphi_0\\
\chi_0
\end{array}\right)=m\left(\begin{array}{c}
\varphi_0\\
\chi_0
\end{array}\right),\]
or
\[(E-e\phi)\left(\begin{array}{c}
\varphi_0\\
-\chi_0
\end{array}\right)-(\bold p-e\bold A)\cdot\left(\begin{array}{cc}
0&\boldsymbol\sigma\\
-\boldsymbol\sigma&0
\end{array}\right)\left(\begin{array}{c}
\varphi_0\\
\chi_0
\end{array}\right)=m\left(\begin{array}{c}
\varphi_0\\
\chi_0
\end{array}\right).\]
We can express this as two explicit equations given by
\begin{align}
(E-e\phi)\varphi_0- \boldsymbol\sigma\cdot(\bold p-e\bold A)\chi_0=m\varphi_0, \label{1}
\end{align}
\begin{align}
-(E-e\phi)\chi_0+\boldsymbol\sigma\cdot(\bold p-e\bold A)\varphi_0=m\chi_0. \label{2}
\end{align}
Equation \eqref{2} can be written as 
\[\chi_0= \frac{\varphi_0}{m+E-e\phi}\boldsymbol\sigma\cdot(\bold p-e\bold A).\]
%\[\frac{\bold{p}^2}{2m}\psi=i\partial_t \psi\]
We consider the non-relativistic and weak field limits hence $m+E-e\phi\approx 2m$ and
\[\chi_0\approx\frac{\boldsymbol\sigma\cdot(\bold p-e\bold A)}{2m}\varphi_0.\]
Substituting into \eqref{1}, we have
\begin{align}
E\varphi_0=\left[\frac{\boldsymbol\sigma\cdot(\bold p-e\bold A)\boldsymbol\sigma\cdot(\bold p-e\bold A)}{2m}+ m+e\phi\right]\varphi_0 .\label{3}
\end{align}
Using the identity \eqref{id}, we obtain the following relation \citep{Armin}
\begin{align}
(\boldsymbol\sigma\cdot\bold A)(\boldsymbol\sigma\cdot\bold B)=\bold A\cdot\bold B+i\boldsymbol\sigma\cdot(\bold A\times\bold B).\label{4}
\end{align}
We can express \eqref{3} as $E\varphi_0=(G+m)\varphi_0$ implying that
\begin{align}
G\varphi_0=\underbrace{\left[\frac{\boldsymbol\sigma\cdot(\bold p-e\bold A)\boldsymbol\sigma\cdot(\bold p-e\bold A)}{2m}+e\phi\right]}_H\varphi_0. \label{5}
\end{align}
Applying \eqref{4} to \eqref{5}
\begin{align}
\boldsymbol\sigma\cdot(\bold p-e\bold A)\boldsymbol\sigma\cdot(\bold p-e\bold A)=(\bold p-e\bold A)^2+i\boldsymbol\sigma\cdot\underbrace{(\bold p-e\bold A)\times(\bold p-e\bold A)}_{**} \label{6},
\end{align}
the term $**$ when simplified yields
\[(\bold p\times\bold A)+(\bold A\times \bold p).\]
Taking the natural constant $\hbar=1$
\[\left[p_i,A_j\right]=-i\partial_iA_j,\]
\[\Longrightarrow (p_iA_j-A_ip_j)+(A_ip_j-p_iA_j)=-i(\partial_iA_j-\partial_jA_i).\]
If we multiply both sides by $\varepsilon_{ijk}$ and sum over $i$ and $j$, we obtain  a third component of $k$ hence \citep{Ryder}
\[\bold p\times\bold A+\bold A\times \bold p=-i\nabla \times \bold A=-i\bold B.\]
Substituting into equation \eqref{6},
\[\boldsymbol\sigma\cdot(\bold p-e\bold A)\boldsymbol\sigma\cdot(\bold p-e\bold A)=(\bold p-e\bold A)^2+i\boldsymbol\sigma\cdot(-i\bold B).\]
We can now write the Hamiltonian in equation \eqref{5} as follows;
\[H=\frac{(\bold p-e\bold A)^2}{2m} -\frac{e}{2m}\boldsymbol \sigma\cdot\bold B+e\phi.\]
The Schrödinger equation now takes a different form expressed as
\[\bigg(\frac{(\bold p-e\bold{A})^2}{2m}-\underbrace{g\frac{e}{2m}\bold{S}}_{\boldsymbol{\upmu}}\cdot\bold{B}+e\phi\bigg)\psi=i\partial_t \psi.\]
This is the \textbf{Pauli equation}. In the equation, the vector operator
$\bold{S}=\tfrac{\boldsymbol{\sigma}}{2}$ is the spin of the electron, $\boldsymbol \upmu$ is the magnetic moment. Hence the constant $g=2$ is the $g$-value of the electron predicted by Dirac equation. 




%\section{The spinors}


%\section{Dirac matrices}
