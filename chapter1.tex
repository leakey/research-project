\chapter{Introduction}\label{ch.1}

The study and measurement of anomalous magnetic moments have provided great insights into the field of subatomic particle physics, the measurement of the proton magnetic moment being one of the first indications of the substructure in the particle \citep{Earl}.

The determination of the anomalous  magnetic moment of the  electron provides a good  way to determine the coupling constant $\alpha$ at $ q^2=0$, in quantum electrodynamics. Its calculation by Schwinger, Feynman and Tomonaga in 1948, and its agreement with data, was a triumph of quantum field theory \citep{Schwartz}. The calculations discussed in this report provide a unique test of the electroweak theory in the standard model of particle physics. The basic concepts and the Dirac equation are discussed in chapters one and two respectively.

In addition, this essay presents a theoretical approach to the determination of the anomalous magnetic moment of the electron as discussed in chapter three. Here, we calculate the magnetic moment by the help of Feynman rules discussed in the same chapter. In conjunction with the existing theory \citep{Griffiths}, these calculations provide an accurate value of the fine structure constant and hence the value of gyromagnetic ratio (\textit{the ratio of the magnetic moment to the spin of electron}), $g$. Some of the applications of electron's magnetic moment and intrinsic spin are discussed in chapter four followed by a brief conclusion in chapter five.

The work discussed here can also be extended to give a better understanding of the CPT (charge, parity time reversal) symmetry as well as Lorentz invariance for leptons in general \citep{Barr}.



\section{Magnetic moment}\label{sec.1}
%Let's demonstrate a figure by looking at Fig.~\ref{bandwidth}. 

In this section, we basically define the meaning of ``anomalous magnetic moment", describe briefly the reason behind its measurements and explain the general ideas involved. We will start by explaining the meaning of the following terms \citep{Franz}:
\begin{enumerate}
\item[$\bullet$] Nuclear spin:- The total intrinsic angular momentum of the nucleus in an atom.
\item[$\bullet$] Gyromagnetic ratio:- The ratio of the magnetic moment to the angular momentum.
\item[$\bullet$] Bohr's magneton:- This is the unit of atomic magnetic moment.
\item[$\bullet$] Lande`s g-factor:- The units of magnetic moment in Bohr's magneton
\end{enumerate}
 
\textbf{Definition:} Magnetic dipole moment,  $\boldsymbol{\upmu}$ is the measure of how much torque, $\boldsymbol{\tau}$ an object experiences in a magnetic field, mathematically expressed as
\[\boldsymbol{\tau}=\boldsymbol{\upmu}\times \bold{B}.\]
The potential energy associated with this force is given as
\[U =-\boldsymbol{\upmu}\cdot\mathbf{B}.\]
For the subatomic particles such as electrons, muons and taus, the magnetic moment is brought about by an intrinsic spin, $\bold{S}$ and the relationship between the two quantities is brought about by a constant known as the gyromagnetic ratio $g$ defined above.
Mathematically, $\boldsymbol{\upmu}$ and $g$ are related as follows:
\begin{equation}
\boldsymbol{\upmu}=g\left(\dfrac{e}{2m}\right)\bold{S}.\label{eqn. 1.1.1}
\end{equation}

For point-like particles, the Dirac theory predicted the value of $g$ to be exactly $2~ (g=2)$ \textit{- this will be revised in section \ref{sec2.3.1}}. However, experiments have revealed a value that is slightly greater than $2$ for charged leptons. This controversy can only be well understood in quantum field theory. 
Studies in quantum field theory have shown that this deviation is as a result of higher order interactions, for point-particles, and rich internal structures for composite particles like protons and neutrons.

The table below shows some data on deviation of the $g-\text{value}$ from $g=2$ for various subatomic particles \citep{Earl}

\begin{table}[!h]
\begin{center}
    \begin{tabular}{ | l | l | l | p{3cm} |}
    \hline
    Particle & Experimental value &Theoretical prediction& Relative precission\\ \hline
    Electron &2.0023193043738 & 2.00231930492 &$4\times 10^{-12}$ \\
   Muon & 2.0023318406 & 2.0023318338& $8\times 10^{-10}$ \\
    Tau & 2.008 & 2.0023546 & $4\times 10^{-2}$ \\
   % Proton & 5.585694674 & 5.58 & $1\times 10^{-8}$ \\
%Neutron & -3.8260854 & -3.72 & $3\times 10^{-7}$ \\    
    \hline
    \end{tabular}
\caption{\textit{Comparison of theoretical and experimental $g$-values of various subatomic particles.}}    
\end{center}
\end{table}
From the table above, we notice that the gyromagnetic ratios of stable and nearly stable particles can be measured to very high precisions. In addition, leptons have $g$-values that can be calculated very precisely in the context of the standard model and the above comparisons gives an important test for theory \citep{Earl}. These comparisons can also help us to test quantum electrodynamics (QED), the theory that is generally considered the most accurate physical theory. Furthermore, we can also introduce Bohr's magneton via the magnetic moment as follows \citep{Earl}

\begin{equation*}
\boldsymbol{\upmu}=g\left(\dfrac{e\hbar}{2m}\right) \frac{\bold{S}}{\hbar}
\end{equation*}

where $\hbar$ is the Planck's constant , $m$ is the mass of the particle and the ratio $\dfrac{e\hbar}{2m}$ is the Bohr's magneton.

\textbf{Classical electromagnetism} predicts $g=1$ if the particle has identical mass and charge distributions. In this case, we assume that the electron's spin is a rotation about its axis of a radius $\mathbf{r}$. If the velocity of the electron is $\mathbf{v}$, the current of density $\mathbf{J}$ flows, and we have:

The current (density):
\[\bold{J}=e\bold{v}.\]
The magnetic moment: \[\boldsymbol{\upmu}=\frac{1}{2} \left(\mathbf{r}\times \mathbf{J}\right)=\frac{e}{2}\left(\mathbf{r}\times\mathbf{v}\right)=\frac{e}{2m}\left(\mathbf{r}\times\mathbf{p}\right),\]
\[\boldsymbol{\upmu}=\frac{e}{2m}\mathbf{L}.\]
%The angular momentum of the particle is\[L=mvr\]

%The magnetic moment, \begin{equation}
%\vec{\upmu}=\left(\dfrac{e}{2m}\right)L
%\end{equation}

Comparing this with equation \eqref{eqn. 1.1.1} above, we readily note that $g=1$ and this implies that classical prediction completely disagrees with the experimental results.

\section{The standard model in a nutshell}\label{sec.2}
According to \cite{Ben}, the universe is basically made up of two major components: matter and energy.
\begin{enumerate}
\item[~]\textbf{Matter:} It makes up 26\% of the universe, 4\% of this is in atoms (3.6\% of which is intergalactic gas) while 22\% of this composition is dark matter.
\item[~]\textbf{Energy:}
The remaining 74\% of the universe volume is dark energy. The gradual expansion of the universe is believed to be as a result of repulsive forces resulting from the dark energy.
\end{enumerate}

From Einstein's equation, $ E=mc^2$, energy can be converted to matter (mass, $m$) and vice versa \citep{Ben}. This section focuses on the standard model that was set up in 1970's, it describes the universe in terms of matter (basically fermions) and force (basically bosons). In this model, we use 17 fundamental particles to describe the other 200 particles of the periodic table. These particles are fermions and bosons.

\subsection*{Fermions}~
%We now discuss the sub-atomic particles that makes up the standard model:

Fermions are leptons and quarks.
%\begin{enumerate}

~~\textit{Leptons}~

We basically have three leptons, and three neutrinos thus making a total of six in number. These are given below:
\begin{table}[!h]
\begin{center}
    \begin{tabular}{ | l | p{3.5cm} |}
    \hline
    Lepton & Neutrino\\ \hline
    Electron($e$) & electron neutrino($\nu_e$)  \\
   Muon($\mu$) & muon neutrino($\nu_\mu$)\\
    Tau($\tau$) & tau neutrino($\nu_\tau$) \\   
    \hline
    \end{tabular}
\caption{\textit{The six leptons with their respective symbols.}}    
\end{center}
\end{table}

Leptons do not reside in the nucleus, and they do not take part in strong interactions. In addition, they lack internal structures and posses a charge of $-1$ while their neutrino counterparts have no charge.

We also note that all leptons exhibits a spin $\frac{1}{2}$ property and what is more, \textit{electrons} are stable while \textit{neutrinos}, \textit{muons} and \textit{taus} are unstable and  decays into leptons and other subatomic particles when isolated \citep{Barr}. For instance;
\[\mu^-\to \overline{\nu}_e +\nu_{\mu}+e^-~~(\text{half  life}=2.20\times 10^{-6}s),\]
\[\tau^-\to \overline{\nu}_{\mu}+\nu_{\tau}+\mu^{-}~~(\text{half life}=2.90 \times 10^{-13}s).\]
Even though the decays occur in a weak interaction, not all weak interaction decays lead to formation of neutrinos \citep{Mano}.

~~\textit{ Quarks}

We generally have six quarks which exists in various flavours, each with a charge that is a fraction of that of the electron \citep{WikBlackScholes}. They are as follows:
\begin{table}[h!]
	\centering
    \resizebox{3.8cm}{!}{
    \begin{tabular}{ | l | l |}
    \hline
    Quark flavour & Charge\\ \hline
    up(u) & $+\frac{2}{3}$  \\
   down(d) & $-\frac{1}{3}$\\
    charm(c) & $+\frac{2}{3}$ \\  
    strange(s)&$-\frac{1}{3}$ \\
    top(t)&$+\frac{2}{3}$\\
    bottom(b)&$-\frac{1}{3}$\\
    \hline
    \end{tabular}}
\caption{\textit{The six flavours of quarks and their respective charges.}}
\end{table}
\FloatBarrier

Quarks can be combined together to form hadrons.This happens in two ways to form the two known types of hadrons:
\begin{itemize}
\item[$\bullet$]Baryons:- These are three-quark hadrons and examples are neutrons (ddu-quarks) and protons (uud-quarks).
\item[$\bullet$]Mesons:- These are made up of a quark and an anti-quark and an example is a pion.
\end{itemize}
Baryons are confined within the nucleus and some like protons are stable while others like neutrons are unstable. The unstable baryons undergo decay when isolated. For example:
\[\underset{\rm uds}{\Lambda ^{\circ}}\longrightarrow\underset{\overline{\rm u}\rm d}{\pi^-}+\underset{\rm uud}{p}.\]

From this example, a three-quark baryon (lambda) decays into another three quark hadron (proton) and a quark-antiquark pion. However, we notice that the strangeness is not conserved.

In contrast, charged mesons are unstable and decay into electrons and neutrinos, while neutral mesons may decay into photons when isolated.
The main difference between the two types of hadrons is that baryons are spin-$\frac{1}{2}$ hadrons while mesons posses integer spins.
%\end{enumerate}

\subsection{Bosons}~

Bosons are integer-spin particles and unlike fermions, they do not obey Pauli's exclusion principle i.e. two or more bosons can occupy the same energy state. We generally have two categories of bosons: gauge bosons and the Higgs boson.

~~\textit{Gauge bosons}

In the standard model, photons ($\gamma$), $W^{\pm}$-bosons, gluons (g) and $Z$-bosons are all categorised as gauge bosons \citep{Ben}. They can be distinguished based on their masses, a property which also determines which type of interactions they are involved in. For instance, photons interacts electromagnetically while $W^{\pm}$-bosons and $Z$-bosons are involved in weak interactions (larger masses). In addition, $W^{\pm}$ can also interact electromagnetically but to a smaller extent. We can therefore deduce that these gauge-bosons generally play a part in electroweak interactions and  their behaviour in interactions can be better understood in Quantum electrodynamic (QED) studies.

There exists a total of \textit{eight} \textit{gluons} that mediate the strong interactions between quarks and just like photons, gluons have little or no mass and are colour sensitive \citep{Griffiths}, a property upon which their strong interactions and interaction  amongst themselves is based. However, their explicit behaviour is described in quantum chromodynamics (QCD) studies which will not be considered here.
\[~~~\]
~~\textit{Higgs boson}~

This type of boson was theoretically predicted in 1964 by Peter Higgs and unlike fermions, it has a spin of zero thus classified together with other integer spin particles \citep{Barr}.
In the standard model, it is used as a reference particle when describing the masses of all other elementary particles. The Higgs boson can participate in weak interaction, electromagnetic interaction as well as individual interactions hence it is extremely unstable \citep{Ben}. Even though experimental attempts to determine its nature have not been successful for sometime, its existence was finally confirmed in March 2013.
 %\subsection{Quantum electrodynamics prediction of the g-value}

\section{Experimental evolution}\label{sec.3}
QED predicts that the anomalous value of $g$ results from vacuum polarisations and fluctuations. In this theory, the relationship between the fine structure constant $\alpha$ and Lande's $g$-factor are represented as an asymptotic series expansion from which the anomalous electron magnetic moment can be deduced
\[g= 2\left[1+k_1\left(\frac{\alpha}{\pi}\right)+k_2 \left(\frac{\alpha}{\pi}\right)^2+k_3\left(\frac{\alpha}{\pi}\right)^3+ \cdots +a_{\mu\tau}+a_{had}+a_{weak} \right].\]  

The constant coefficients and the term $a_{\mu\tau}$ have been calculated while the other terms are non-QED polarisations and weak observables \citep{Odom}. The fine structure constant $\alpha$ is given by

\[\alpha=\frac{e^2}{4\pi\epsilon_0\hbar c}\approx \frac{1}{137.036}.\]

Several experiments have been done over years beginning 1953, with the universal motive to determine the possible internal structure of an electron and a possible interaction with the vacuum polarisations of QED. These experiments involve use of cyclotrons - \textit{a type of particle accelerator in which charged particles are accelerated outwards by a rapidly changing electric field while being held from the centre along a spiral path by a static magnetic field}, and a penning trap, in which an electron is considered to be a mechanical system with equal distributions of charge and mass \citep{Earl}. The value of $g$ is then calculated directly by use of measured cyclotron frequency and spin frequencies, taking into consideration the possible error sources in these experiments.

\begin{figure}[h!]
\begin{center}
\includegraphics[scale=0.4]{penning.png}
\caption{The cross-sectional area of the penning trap   \citep{Odom}}
\end{center}
\end{figure}

The idea of measuring $g-2$ instead of $g$ was developed in 1953 when the first experimental attempt to determine $g$-value was made. These series of experiments were done in the University of Michigan \citep{Odom}. It involved placing a number of electrons in a magnetic mirror  trap and then studying the relative orientation of the precessing spin and the orbital angular moment vectors. The result from this experiment revealed the value of $g$ to be $g\approx 2.002319$.

Between 1977 and 1999, other experiments were done on a similar set up at the University of Washington and the outcome was a great improvement in the previous work, the experiment was improvised to detect only single electrons in the penning traps.

In 1987, the improvement in the experiment involved use of molybdenum electrodes in the penning traps, and the outcome was $g\approx 2.002313$. This was a great milestone as it was considered to be a more accurate value. However, it had a limitation of non-reliable data values and inability to achieve finer line widths \citep{Odom}.
The same experiments were repeated with a positron and it resulted into a different value of $g\approx 2.002319$. The inaccuracy was as a result of drive-shift and cavity-shift systematics \citep{Odom}.

In the early 1990's, the molybdenum electrodes were replaced with phosphor-bronze electrodes in order to reduce the shifts that were initially a limitation, the modifications yielded a result that could not show a Gaussian distribution when plotted.

The latest experiments were established in Harvard University (2004). The aim is to improve the existing results and also to probe the $g-\rm value$ of other subatomic particles such as muons. The value from this Harvard experiment (2004) stood at
$g=2.00231930436~(57)$ \citep{Earl}. This is a better result since it is very accurate, and  almost in agreement with the result obtained from the QED calculations.
