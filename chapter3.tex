
\chapter{The vertex corrections}\label{ch.3}
In this chapter, we discuss the modification of the vertex by a one-loop Feynman diagram with an internal photon that eventually leads to the calculations of the anomalous electron magnetic moment in the momentum space. What is more, we discuss the second order correction to an electromagnetic vertex. 
%%\chapter{Third Chapter}

\section{The Feyman rules}\label{sec.3.1}
In this section, we introduce the Feynman rules for calculating the scattering matrix $\mathcal{M}$ in the QED theory. The diagrams upon which these rules are based can either be tree or closed diagrams with one or more loops. For our studies, we shall scale down to the specific rules that would lead us to the calculation of $g$-value from a one-loop correction.

Every Feynman diagram is made up of lines and vertices whereby the straight lines represents a fermion while a wavy line represents a photon with each line propagated from one space-time location to another. The particle propagation from every point are assigned four-momenta which is conserved at the point of interaction(\textit{vertex}). In a nutshell, the rules are summarised as follows:-

\begin{enumerate}
\item[$\bullet$]According to \cite{Griffiths}, notations of the incoming and outgoing four-momentum $p$ and $p'$ as well as the internal momenta $q$ and $q'$ must be done appropriately, and in order to keep track of direction, arrows must be used.
\item[$\bullet$] At every vertex, each particle's momentum must be multiplied by a factor $i$  \citep{Franz}.
\item[$\bullet$]For a photon with a polarisation index of $\mu$, interacting with other particles at a vertex,
%\begin{wrapfigure}{r}{0.1\textwidth}
%\includegraphics[scale=0.3]{photon_vertex.png}
%%\caption{a photon absorbed or emitted at a vertex}
%\end{wrapfigure}
we use a factor $\pm ie \gamma_\mu$ which is either absorbed or emitted by a fermion of charge $\pm e$ respectively.
\item[$\bullet$]A closed fermion loop is assigned a factor of $(-1)$ and an opposite sign for the same fermion term applies if the fermion lines are exchanged \citep{Armin}.
\item[$\bullet$]The momentum is always conserved at every vertex and the undetermined 4-momenta are integrated over $k$  if not fixed by energy-momentum conservation with the vertex weight \cite{Griffiths}, i.e.
\[\int \frac{d^4 k}{(2\pi)^4}.\]
%\cite{Franz} gives the following additional rules for the treatment of the fermions, momentum-carrying photons and spin $\frac{1}{2}$ particles.
\item[$\bullet$]An internally produced momentum-carrying photon (\textit{propagator}) with a momentum $k$ and polarisation indices $\mu$ and $\nu$ is represented with a factor \citep{Franz}:
\begin{align*}
i\Lambda_P^{\mu\nu}(k)=\frac{ig^{\mu\nu}}{-k^2-i\epsilon}\Rightarrow\begin{tikzpicture}[very thick][decoration=snake]
\draw[draw=blue, >=stealth, ->, snake=snake,segment amplitude = .7mm, segment length = 5mm] (2,0) -- (3,0);
\draw[draw=blue, >=stealth, -, snake=snake,segment amplitude = .7mm, segment length = 5mm] (3,0) -- (4,0);
\end{tikzpicture}.
\end{align*}
\item[$\bullet$]For an internal fermion of momentum $p$, we have a factor of:
\begin{align*}
\frac{i( \slashed{p} +m)}{p^2-m^2+i\epsilon}\Rightarrow \alpha\begin{tikzpicture}[very thick][decoration=snake]
\draw[draw=purple,
        decoration={markings, mark=at position 0.625 with {\arrow{>}}},
        postaction={decorate}
        ] (2,0) -- (4,0);
\end{tikzpicture}\rho.
\end{align*}
\item[$\bullet$]For fermions, we assemble the incoming and outgoing fermion spinors, vertex operators, along each fermion line in order to make a matrix element.
\end{enumerate}

\section{Virtual particles}\label{3.2}
In the study of the sub-atomic particles, we encounter the interactions of the force carrying particles at the microscopic level.
For example, excitation of the electromagnetic fields between particles mediates the resultant Coulomb force between them. In this case, the distance between the interacting(repulsive/ attractive)   particles is very difficult to visualize and therefore the process can only be possible as a result of exchange of some unseen photons.

\begin{wrapfigure}{r}{0.2\textwidth}
\tikzset{
particle/.style={draw=purple, postaction={decorate},
    decoration={markings,mark=at position .5 with {\arrow[purple]{triangle 45}}}},
gluon/.style={decorate, draw=blue,
    decoration={coil,aspect=0}}
}
\begin{tikzpicture}[node distance=0.5cm and 1.25cm]
\coordinate[label=left:$e^{-}$] (e1);
\coordinate[below right=of e1] (aux1);
\coordinate[above right=of aux1,label=right:$e^{-}$] (e2);
\coordinate[below=1.25cm of aux1] (aux2);
\coordinate[below left=of aux2,label=left:$e^{-}$] (e3);
\coordinate[below right=of aux2,label=right:$e^{-}$] (e4);
\draw[particle] (e1) -- (aux1);
\draw[particle] (aux1) -- (e2);
\draw[particle] (e3) -- (aux2);
\draw[particle] (aux2) -- (e4);
\draw[gluon] (aux1) -- node[label=right:$\gamma-virtual\, photon$] {} (aux2);
\end{tikzpicture}
\caption{\textit{A virtual particle in an elastic electron scattering}}
\end{wrapfigure}
There is therefore need to understand where the photons originates, and where they go to afterwards.

The mediating photons are emitted by one electron and absorbed by another. It is not easy to see which electron emits or which ones absorbs but the effects of the processes involved can tell. The force carrying particles (messenger particles) are the \textit{virtual particles}. They exhibits properties different  from those of the real particles.

These unusual properties of virtual particles can be illustrated by considering a single virtual photon involved in an elastic scattering \citep{Franz}. If we assume that the nucleus is more massive, it appears to be approximately stationary. If the incoming electron has a momentum $\bold{p}$, and the outgoing electron has a momentum $\bold{p'}$.
When the electron of momentum $\bold{p}$ absorbs the virtual photon, it experiences a change in momentum given by 
$\delta \bold{p}=\bold{p'}-\bold{p}$ and unchanged energy  i.e. $ E'=E$.

According to \cite{Barr}, the final energy and momentum of the photon  after the process is given by:
$E_{\gamma}=0 $ and $\bold{p_{\gamma}}=\bold{p'}-\bold{p}=\delta \bold{p}$ respectively. From this, we observe that 
$E_{\gamma}^2\neq p_{\gamma}^2$ (off-shell). 
What is more, they exhibit an energy-momentum invariance which is not equal to the square of the mass

\[p.p=E^2-\bold{p}\cdot\bold{p}\neq m^2.\]

They are therefore said to be ``off mass shell''.

\section{Electron's magnetic moment and the Lande's g-factor} \label{sec.3.3}

If we consider a  particle scattering from another heavier particle, the strength of the electromagnetic constant can be measured by considering a low energy elastic scattering process called Mott scattering \cite{Naga}  and in this case, the matrix element of the process can be written as follows

\begin{align}
i\mathcal{M}=\overline{u}(p')ie \Gamma_{\nu} u(p) i\frac{g^{\mu\nu}}{q^2}\overline{u}(k')ie\gamma_{\mu}u(k),\label{3.3.1}
\end{align}

where $\overline{u}(p')$ and $u(k)$ are the solutions to Dirac equations for the scattering electron and the heavier particle respectively and $\Gamma_{\nu}$ is a representation of all the vertex corrections.
For the case of this study, we consider the process that provides the leading order corrections to the vertex.

\begin{wrapfigure}{r}{0.2\textwidth}
%\begin{center}
\includegraphics[scale=0.3]{vertex.png}
%\end{center}
\caption{\textit{General vertex}}
\end{wrapfigure}
In the diagram, $p$ and $p'$ are the momenta of the incoming and scattered electrons respectively and $q=p'-p$ is the momentum of the virtual photon discussed in  section \ref{3.2} above. The goal now is to compute the vertex corrections $\Gamma_{\nu}$

In order to attain the goal, we need to carry out an explicit loop computation for $\Gamma_{\nu}$. The form that it takes can be evaluated using symmetry properties e.g. Lorentz transformation. In this case, $\Gamma_{\mu}=\gamma_{\mu}$ for the lowest order.

In general, $\Gamma^{\mu}=\Gamma^{\mu}(\gamma^{\mu},p^{\mu},p'^{\mu})$. Hence we can express it as a linear combination of other scalar functions $A(p,p')$, $B(p,p')$ and $C(p,p')$ \citep{Prasanta}  i.e.

\begin{align}
\Gamma^{\mu}=\gamma^{\mu}A(p,p')+(p^{\mu}+p'^{\mu})B(p,p')+(p^{\mu}-p'^{\mu})C(p,p').\label{3.3.2}
\end{align}

From here, we can get the exact corrections by evaluating the values of scalar functions $A,B$ and $C$ to all orders.
Next, we apply a Ward-Takahashi identity for a three-point vertex function (since it holds even if the photon is off-shell) \citep{Naga}.
\begin{align*}
q_{\mu}\Gamma^\mu=0.
\end{align*}
\begin{proof}~
To show that $C(p,p')=0$, we apply
Ward -Takahashi identity and Lorentz conservation laws to equation \eqref{3.3.2} and hence we have:
\[q_{\mu}\Gamma^\mu=\slashed{q}A+q\cdot(p+p')B+q\cdot(p-p')C=0.\]
Since $q=p'-p$ and $p'^2=p^2=m^2$ (on-shell electrons), we note that
\[q\cdot(p+p')=0.\]
From Dirac equation, $(\slashed{p}-m)u(p)=0 $ hence,
\[\overline{u}(p')\slashed q u(p)=\overline{u}(p')(\slashed{p'}-\slashed{p})u(p)=\overline{u}(p')(m-m) u(p)=0,\]
For the last term,
\[q\cdot(p-p')=-q^2\]
$\Longrightarrow p'^2=p^2+2pq+q^2$, hence $q^2+2pq=0$, and from here, we see  that $q^2\neq0$.

Therefore, the term $q\cdot(p-p')\neq0$, implying that $C=0$ for equation \eqref{3.3.2} to be consistent.
\end{proof}

Substituting $C(p,p')=0$ into equation \eqref{3.3.2}, we have the final correction function expressed as
\begin{align}
\Gamma^{\mu}=\gamma^{\mu}A(p,p')+(p^{\mu}+p'^{\mu})B(p,p')\label{3.3.3}
.\end{align}

We now apply Gordon identity to rewrite $p^{\mu} $and $p'^{\mu}$ dependence in terms of $\sigma^{\mu\nu}$ since it holds for on-shell spinors and this results to
\begin{equation}
\overline{u}(p')\gamma^{\mu}u(p)=\overline{u}(p')\left[\dfrac{p^{\mu}+p'^{\mu}}{2m}+i\dfrac{\sigma^{\mu\nu}}{2m}q_{\nu}\right]u(p).\label{3.3.4}
\end{equation}
In the above equation, $\sigma^{\mu\nu}=\dfrac{i}{2}[\gamma^{\mu},\gamma^{\nu}] $ is the generator for the Lorentz transformation.
Since the $p'$ and $p$ dependence is only brought about by $q^2$, we can express equation \eqref{3.3.4} as

\begin{align}
\Gamma^{\mu}=\gamma^{\mu}F_1(q^2)+i\frac{\sigma^{\mu\nu}}{2m}q_{\nu}F_2(q^2),\label{3.3.5}
\end{align}
where $F_1$ and $F_2$ are some functions of $q^2$ only in order to maintain Lorentz covariance. They are known as \textit{form factors} and $F_1$ shows the effect of all corrections to the charge (charge re-normalization) while $F_2$ illustrates the effect of corrections to the magnetic moment.
%\subsection{Computing the form factors}
%\paragraph{ }
Now, we have to find the form factors by computing the  loop correction.

In the non-relativistic limit, where the momentum transferred from the heavy particle $q\to 0$ (since the magnetic field is weak), $F_1(q^2)$ will not receive any corrections whereas $F_2(q^2)$ will receive non-trivial corrections \citep{Franz}.
If we consider an electron scattering from an external  electromagnetic field, we assume a static vector potential so that we have only an external magnetic field. The interaction Hamiltonian can therefore be expressed as 
\[H_{int}=\int d^3x eA_{\mu}^{cl}j^{\mu},\] where $j^{\mu}=\overline{\psi}(x)\gamma^{\mu}\psi(x)$
. We take a classical potential $A_{\mu}^{cl}$ since the external field is not quantised. The Feynman amplitude for the leading order correction can now be computed as
\begin{align*}
i\mathcal{M}(2\pi) \delta(p'_0-p_0)=-ie\overline{u}(p')\gamma^{\mu}u(p)\widetilde{A}_{\mu}^{cl}(p'-p),
\end{align*}
where  $\widetilde{A}_{\mu}^{cl}$ is the Fourier transform of $A_{\mu}^{cl}$.
For our case, $ \gamma^{\mu}$ is represented by $\Gamma^{\mu}$ and the potential $\widetilde{A}_{\mu}^{cl}(x)=(0,\bold{A})$ hence the amplitude is given by
\begin{align*}
i\mathcal{M}= \overline{u}(p')ie\Gamma^{\mu}u(p)\widetilde{A}_{\mu}^{cl}(\bold{q})= \overline{u}(p')ie\left[\gamma^{i}F_1(q^2)+i\frac{\sigma^{i\nu}}{2m}q_{\nu}F_2(q^2)\right]u(p)\widetilde{A}_{cl}^{i}(\bold{q}),
\end{align*}
where 
$\widetilde{A}_{cl}^{i}(\bold{q})=-\widetilde{A}_{cl}^{\mu}(x)$. 
In the non-relativistic limit, where $q$ is very small,
\begin{align}
i\mathcal{M}= \overline{u}(p')ie\left[\gamma^{i}F_1(0)+i\frac{\sigma^{i\nu}}{2m}q_{\nu}F_2(0)\right]u(p)\widetilde{A}_{cl}^{i}(\bold{q}).\label{3.3.6}
\end{align}
We now take the explicit solution of Dirac equation generally expressed as equation \eqref{2.2.6}, 
where the two component spinors discussed above are now represented by $\varphi(0)$.
%\begin{align}
%u(p)=\frac{\slashed{p} +m}{\sqrt{ 2m(m+E)}}u(0)=\left(
%\begin{array}{c}
% \sqrt{\frac{E+m}{2m}} \varphi(0)\\
%\frac{\vec{\sigma}.\vec{p}}{\sqrt{2m(E+m)}}\varphi(0)
%\end{array}\right)
%\end{align}
We will now work in a representation where the $\gamma$-matrices are given by equation \eqref{2.1.8}
and assume that the momenta are all small compared to the mass of the electron.
Therefore, the first term in equation \eqref{3.3.6} can be expressed as
%\[\gamma^0=\left(\begin{array}{cc}
%I&0\\
%0&-I
%\end{array}\right); \gamma^{i}=\left(\begin{array}{cc}
%0&\sigma^i\\
%-\sigma^i&0
%\end{array}\right)\] 
\begin{align*}
\overline{u}(p')\gamma^{i}u(p)=u^{\dagger}(p')\gamma^{0}\gamma^{i}u(p)=u^{\dagger}(p')\left(\begin{array}{cc}
0&\sigma^i\\
\sigma^i&0
\end{array}\right)u(p).
\end{align*}
This implies that
\begin{align*}
\overline{u}(p')\gamma^{i}u(p)=\left(\begin{array}{cc}
\varphi '^{\dagger}(0)\sqrt{\frac{E'+m}{2m}} &\varphi '^{\dagger}(0)\frac{\boldsymbol{\sigma}\cdot\bold{p'}}{\sqrt{2m(E'+m)}}
\end{array}\right)\left(\begin{array}{cc}
0&\sigma^i\\
\sigma^i&0
\end{array}\right)\left(\begin{array}{c}
 \sqrt{\frac{E+m}{2m}} \varphi(0)\\
\frac{\boldsymbol{\sigma}\cdot\bold{p}}{\sqrt{2m(E+m)}}\varphi(0)
\end{array}\right)
\end{align*}
after multiplying out and simplifying using the assumption  that at non-relativistic limit $E'=E$ we get
\begin{align*}
\overline{u}(p')\gamma^{i}u(p)=
\frac{1}{2}\varphi '^{\dagger}(0)\left[\boldsymbol{\sigma}\cdot\bold{p'}\sigma^i+\sigma^i\boldsymbol{\sigma}\cdot\bold{p} \right]\varphi (0).
\end{align*}
We now apply the commutator-anti commutator relation identity (\ref{id}) %$\boxed{\sigma^{i}\sigma^{j}=\delta^{ij}+i\varepsilon^{ijk}\sigma^k}
to simplify the equation further, the equation can be rewritten as
\[\overline{u}(p')\gamma^{i}u(p)=
\frac{1}{2}\varphi '^{\dagger}(0)\left[\sigma^{j}\sigma^ip'^{j}+\sigma^i\sigma^{j} p^{j} \right]\varphi (0),\]\begin{align*}\overline{u}(p')\gamma^{i}u(p)=\frac{1}{2}\varphi '^{\dagger}(0)\left[(p'+p)^i+i\varepsilon^{jik}\sigma^k(p'-p)^{j} \right]\varphi (0).
\end{align*}

On further simplification, we only consider the terms that are linear in $q_j$ and hence we obtain the \textbf{first term} to be
\begin{align*}\overline{u}(p')\gamma^{i}u(p)=\varphi '^{\dagger}(0)\left[-\frac{1}{2}i\varepsilon^{ijk}q^{j} \sigma^k\right]\varphi (0)+\cdots.
\end{align*}
For the second term, we need to evaluate
\begin{align}\overline{u}(p')\sigma^{i\nu}q_{\nu}u(p)=\overline{u}(p')\sigma^{ij}q_{j}u(p)+\cdots.\label{3.3.8}
\end{align}
In the standard Weyl representation, $\sigma^{ij}=\frac{i}{2}[\gamma^i,\gamma^j]=\varepsilon^{ijk}\left(\begin{array}{cc}
\sigma^k & 0\\
0& \sigma^k
\end{array} \right)$. We now multiply both sides of the equation \eqref{3.3.8} by $\frac{i}{2m}$ and take only the terms that are linear in $q_j$
\begin{align*}
\frac{i}{2m}\overline{u}(p')\sigma^{ij}q_{j}u(p)+\cdots=\frac{i}{2m}u^{\dagger}(p')\left(\begin{array}{cc}
I & 0\\
0& -I
\end{array} \right)\left(\begin{array}{cc}
\sigma^k & 0\\
0& \sigma^k
\end{array} \right)u(p)\varepsilon^{ijk}(-q^j)+\cdots.
\end{align*}
On further simplification using the procedures above, we obtain the \textbf{second term} as
\begin{align*}
\frac{i}{2m}\overline{u}(p')\sigma^{i\nu}q_{\nu}u(p)=\varphi'^{\dagger}(0)\left[-\frac{i}{2m}\sigma^k \varepsilon^{ijk}q^j\right]\varphi(0)+\cdots.
\end{align*}

We have now evaluated the terms in the Feynman amplitude and therefore we substitute them back into the original equation \eqref{3.3.6} to get
\begin{align}
i\mathcal{M}=-ie(2m)\varphi'^{\dagger}(0)\left(-\dfrac{1}{2m}\sigma^k\big(F_1(0)+F_2(0)\big)\underbrace{i\varepsilon^{ijk}q^j\tilde{A}_{cl}^i
(\bold{q})}_*\right)\varphi(0).
\end{align}
 Since $\mathbf{B}(x)=\nabla\times \mathbf{A}(x)$. We notice that the term marked \textbf{*} is the Fourier transform of $\mathbf{B}^k(x)$ which we can express as 
 $i\varepsilon^{ijk}q^j\tilde{A}_{cl}^i
(\bold{q})=\tilde{B}^k(\bold{q})$
and the amplitude is now expressed as
\begin{align}
i\mathcal{M}=-ie(2m)\varphi'^{\dagger}(0)\left(-\dfrac{1}{2m}\sigma^k\big(F_1(0)+F_2(0)\big)\right)\tilde{B}^k(\bold{q})\varphi(0)\label{3.3.10}
.\end{align}
According to \cite{Karolos}, the Born approximation of the scattering amplitude is given by the
Fourier transform of the scattering potential, hence we can write the Born approximation of equation \eqref{3.3.10} as
\begin{align*}
U=-\langle \boldsymbol{\upmu}\rangle\cdot \bold{B}.
\end{align*}
This implies that
\begin{align*}
\langle \boldsymbol{\upmu}\rangle=\frac{e}{2m} 2\big(F_1(0)+F_2(0)\big)\varphi'^{\dagger}(0)\frac{\boldsymbol{\sigma}}{2}\varphi(0).
\end{align*}
We now introduce a constant $g$ (Landes' $g$-factor) and the spin operator $\bold{S}=\varphi'^{\dagger}(0)\dfrac{\boldsymbol{\sigma}}{2}\varphi(0)$ to obtain
\begin{align}
\langle \boldsymbol{\upmu}\rangle=g\left(\frac{e}{2m}\right)\bold{S}.\label{3.3.11}
\end{align}

From this equation \eqref{3.3.11}, we can deduce the value of $g$ as
\begin{align*}
g=2\big[F_1(0)+F_2(0)\big].
\end{align*}

In a 1-loop correction, $F_1(0)=1$ since it doesn't receive any corrections, implying that:

\begin{equation}
g=2+2F_2(0).\label{3.3.12}
\end{equation}


The first term, 2 represents the value of $g$ predicted by the Dirac theory (without corrections) while the second term $2F_2(0)$ is the anomaly resulting from the corrections explained in quantum theory. The main task is now to determine $F_2(0)$ and hence the anomalous magnetic moment.

\section{Calculation of the anomalous magnetic moment  ($2F_2(0)$)}\label{sec.3.4}

As stated above, we will calculate $2F_2(0)$ from a 1-loop correction to the vertex vector by the help of the diagram shown

\begin{wrapfigure}{l}{0.2\textwidth}
%\begin{center}
\includegraphics[scale=0.3]{Calc.png}
%\end{center}
\caption{\textit{Feynman diagram for a 1-loop correction }}\label{fig 3.3}
\end{wrapfigure}

Using the Feynman identity for the amplitude and external electron states, we can express the  amplitude as an integral over the product of individual corrections at the vertices 1, 2 and 3 and then integrate over $k$ \citep{Schwartz,Franz}. This is given by
\[i\mathcal{M}_\Gamma=ie\overline{u}(p')\Gamma^\mu u(p)\]

\begin{align*}
i\mathcal{M}_\Gamma=\overline{u}(p') \dfrac{(-ie)^3}{(2\pi)^4}\int d^4k \left[\dfrac{(-1)\gamma^{\alpha}(g_{\nu\alpha})}{\beta^2-k^2-i\epsilon}\right]\left[\frac{\gamma^{\mu}(m+\slashed{p}-\slashed{k})}{m^2-(p-k)^2-i\epsilon}\right]\left[ \frac{\gamma^{\nu}(m+\slashed{p'}-\slashed{k})}{m^2-(p'-k)^2-i\epsilon}\right]u(p)
\end{align*}

where $\beta$ is the arbitrary mass of the virtual photon we introduce to prevent any contributions from very soft photons which could otherwise lead to infra-red divergences. The photon is emitted at 1 and reabsorbed at 3
figure \ref{fig 3.3}. 
To simplify the equation, we now use the Dirac equation as in \citep{Franz}
\[\slashed{p}u(p)=mu(p)~\text{and} ~u(p)\slashed{p'}=mu(p).\]

We therefore have
\begin{align}
i\mathcal{M}^{\mu}=\overline{u}(p') \frac{ie^2}{(2\pi)^4}\int d^4k \frac{\gamma_{\nu}(m+\slashed{p}-\slashed{k})\gamma^{\mu}(m+\slashed{p'}-\slashed{k})\gamma^{\nu}}{[m^2-(p-k)^2-i\epsilon ][m^2-(p'-k)^2-i\epsilon]  [\beta^2-k^2-i\epsilon]}u(p).\label{3.4.1}
\end{align}
According to \cite{Franz}, a 1-loop integral of the above form can be  evaluated by:
\begin{enumerate}
\item[$\bullet$] Combining the denominators into a single denominator and then reducing to the standard form by shifting the loop momenta. 
\item[$\bullet$]The standard integral can then be evaluated by use of an identity, generally expressed as
\begin{align}
\frac{1}{ABC}=\int_{0}^1 dx_1 \int_0^{1-x_1}dx_2\frac{2}{\big[x_1A +Bx_2+C(1-x_1-x_2)\big]^3}.
\end{align}
\end{enumerate}
%\begin{align*}
%\boxed{\frac{1}{A_1A_2A_3...A_n}=\int_{0}^1 dx_1dx_2dx_3...dx_n\delta(\Sigma_{x_i}-1)\frac{(n-1)!}{(x_1A_1x_2A_2+...x_nA_n)^n}}
%\end{align*}
%For our case, we have only 3 terms at the denominator, hence the identity simplifies to:
Now, since both the incoming and the outgoing electrons are on-shell, $p'^2=p^2=m^2$ hence,
$$A=m^2-(p'-k)^2-i\epsilon=2p'.k-k^2-i\epsilon$$
 \[B=m^2-(p-k)^2-i\epsilon=2p.k-k^2-i\epsilon\]
 \[C=\beta^2-k^2-i\epsilon.\]
These are the Feynman's parameters. If $D$ is the denominator and $N^\mu$ is the numerator, we write
 \[D=-k^2+ 2(x_2p+x_1p').k+ \beta^2(1-x_1-x_2)-i\epsilon.\] We will now complete the square in $D$ by shifting $k$ such that $k=k'+x_1p'+x_2p$ while at $N^\mu$ we shift $k\rightarrow k' $ and then substitute back \cite{Franz}, hence  we have 

\begin{align*}
D=(x_1^2+x_2^2)m^2+ 2x_1x_2p.p'+\beta^2(1-x_1-x_2)-k'^2-i\epsilon
\end{align*}
and
\begin{align*}
N^\mu=\gamma^\nu \big[m+\slashed{p'}(1-x_1)-x_2\slashed{p}-\slashed{k'}\big]\gamma^{\mu}\big[m+\slashed{p}(1-x_2)-x_1\slashed{p'}-\slashed{k'}\big]\gamma_\nu.
\end{align*}
Since $D$ is an even function of $k'$, we now drop the terms in $N^{\mu}$ that are linear in $k'$ as they will integrate to zero. We therefore have:

\begin{align*}
N^\mu=\gamma^\nu \big[m+\slashed{p'}(1-x_1)-x_2\slashed{p}\big]\gamma^{\mu}\big[m+\slashed{p}(1-x_2)-x_1\slashed{p'}\big]\gamma_\nu+ \gamma^\nu\slashed{k'}\gamma^{\mu}\slashed{k'}\gamma_\nu
.\end{align*}
We will now use the identities of the $\gamma$ matrices listed below \citep{Kresimir}
\begin{enumerate}
\item[$\bullet$]$\slashed{p}\slashed{p}=p.p.I$.
\item[$\bullet$]$\gamma^{\mu}\gamma_\mu=4I $.
\item[$\bullet$]$\gamma^{\mu}\slashed p\gamma_\mu=-2\slashed p $.
\item[$\bullet$]$\gamma^{\mu}\slashed p\slashed q \gamma_\mu=4p.q $.
\item[$\bullet$]$\gamma^{\mu}\slashed p\slashed q  \slashed r\gamma_\mu=-2\slashed r \slashed q p $.
\item[$\bullet$] $\slashed{q}\gamma^\mu\slashed{q} =2q^\mu\slashed{q}-\gamma^\mu q^2$.
\end{enumerate}
We can now apply the above identities to reduce the numerator to
\[N^\mu=-2m^2\gamma^{\mu} +4m\big[p'^{\mu}(1-x_1)-x_2p^{\mu}+p^{\mu}(1-x_2)-x_1p'^{\mu}\big]\]
\begin{equation*}-2\big[\slashed p(1-x_2)-x_1\slashed  p')\gamma^{\mu}(\slashed  p'(1-x_1)-x_2\slashed  p\big]-2\slashed  k'\gamma^{\mu}\slashed  k'.
\end{equation*}
 We now use the identities $\slashed  p=\slashed  p'-\slashed q$ and $\slashed {q}\gamma^\mu\slashed {q} =2q^\mu\slashed {q}-\gamma^\mu q^2$ together with algebraic simplifications. According to \cite{Franz}, the $x_2-x_1$ terms integrates to zero as $q\to 0$  and  the result is given as

\[N^\mu=\gamma^{\mu}\big(-2m^2-2m^2(1-x_1-x_2)^2-2(1-x_2)(1-x_1)q^2+2k'^2\big)-4k'^{\mu}\slashed  k'+4m(1-x_1-x_2)(p'+p)^{\mu}\]
\begin{equation*}
-2m(1-x_1-x_2)(1-\frac{1}{2}(x_1+x_2))[\gamma^{\mu},\slashed  q].
\end{equation*}

For further simplification, we use the identities
$\frac{i}{2}[\gamma^{\mu},\slashed  q]=\sigma^{\mu\nu}q_\nu$ and $p'^\mu+p^\mu =2m\gamma^\mu-i\sigma^{\mu\nu}q_\nu$, hence
\begin{equation*}
N^\mu=\gamma^{\mu}\bigg(-2m^2(1-x_1-x_2)^2+\big(1-4(1-x_1-x_2)\big)-2q^2(1-x_1)(1-x_2)+2k'^2\bigg)-i2m\sigma^{\mu\nu}q_\nu(1-x_1-x_2)(x_1+x_2).
\end{equation*}
Substituting $D$ and $N^\mu$ into the equation \eqref{3.4.1}, we have
\begin{equation}
i\mathcal{M}^\mu=\overline{u}(p')2ie^2\int_{0}^1dx_1\int_{0}^{1-x_1}dx_2\int \frac{d^4k'}{(2\pi)^4}\frac{N^\mu}{\big[D(k')\big]^3}u(p).
\end{equation}

Comparing this form of amplitude to that of equation \eqref{3.3.5} and  taking into account the form of $N^\mu$, we notice that $i\mathcal{M}^\mu$ can be conveniently written in terms of $F_1(q^2)$ and $F_2(q^2)$ terms. However, we have already seen for this case that $F_1(q^2\to 0)=1$ and we need now to compute $F_2(q^2\to 0)$. In order to compute this term, we consider the terms that are  a coefficient of $i\sigma^{\mu\nu}q_\nu$ at the numerator $(N^{\mu})$. This is expressed as
\begin{equation*}
F_2(q^2)=-2ie^2\frac{1}{(2\pi)^4}\int d^4k' \int_{0}^1dx_1\int_{0}^{1-x_1}\frac{4m^2(x_1+x_2)(1-x_1-x_2)}{\big[D(k')\big]^3} dx_2
\end{equation*}
%We now recall that $D$ can also be written as
%\[D=(x_1+x_2)^2m^2-2x_1x_2q^2+\beta^2(1-x_1-x_2)-k'^2-i\epsilon.\]
%,
where at $q^2=0$,
\[D=(x_1+x_2)^2m^2+\beta^2(1-x_1-x_2)-k'^2-i\epsilon\]
so that
\begin{equation}
F_2(0)=-2ie^2\frac{1}{(2\pi)^4}\int d^4k' \int_{0}^1dx_1\int_{0}^{1-x_1}\frac{4m^2(x_1+x_2)(1-x_1-x_2)}{\big[(x_1+x_2)^2m^2+\beta^2(1-x_1-x_2)-k'^2-i\epsilon]^3} dx_2.\label{3.4.4}
\end{equation}
For further simplification, we use the identity \citep{Schwartz}
\[\frac{i}{(2\pi)^4}\int d^4k' \frac{1}{[-k'^2+\bigtriangleup-i\epsilon]^3}=\frac{-1}{32\pi^2\bigtriangleup^2}\] where, for our case, \[\bigtriangleup=(x_1+x_2)^2m^2+\beta^2(1-x_1-x_2).\]
According to \cite{Franz}, the singularity at 
$x_1+x_2=0$ is only a point in a two-dimensional space hence integrable. If the virtual photon's mass $\beta\to 0$ and the coupling constant $\alpha=\dfrac{e^2}{4\pi}$, we can change the variables as follows
\begin{align*}
\kappa= x_1 + x_2 ~~ \text{and} ~~  \Omega=\frac{x_1-x_2}{2\kappa}.
\end{align*}

The volume element then transforms to \[\int_{0}^1dx_1\int_{0}^{1-x_1}
dx_2=\int_{0}^1\kappa d\kappa\int_{-\frac{1}{2}}^{\frac{1}{2}}
d\Omega.\]
Therefore we can write equation \eqref{3.4.4} as
\begin{equation}
F_2(0)=\frac{\alpha}{\pi}\int_{0}^1 d\kappa\int_{-\frac{1}{2}}^{\frac{1}{2}}
(1-\kappa)d\Omega=\frac{\alpha}{2\pi},\label{3.4.5}
\end{equation}
\[2F_2(0)=\dfrac{\alpha}{\pi} .\] 
This is \textbf{the anomalous electron magnetic moment}. In this case, the coupling constant is the fine structure constant ($\alpha\approx \dfrac{1}{137}$). By combining equation \eqref{3.3.12} and equation \eqref{3.4.5}, we obtain the gyromagnetic ratio for the electron from the 1-loop correction, expressed as
\begin{equation}
g=2+\frac{\alpha}{\pi}\approx 2.00232.
\end{equation}
The corrections of higher order can be calculated in the same way by use of more complex Feynman diagrams. However, the 1-loop correction we have used was a great milestone historically in the quantum electrodynamic field theory, it actually convinced people that loops have physical effects.
\section{Discussion of higher order corrections}\label{sec.3.5}

Higher order corrections includes vertex corrections (calculated above), photon propagator corrections, fermion propagator corrections as well as corrections due to emission of very low energy photons. For diagrams with loops, two types of divergences of the integrals  generally arises when calculating these higher corrections using Feynman diagrams, these are \textbf{ultra-violet divergence} (i.e. the integral diverges like $\sim \log k^2$ as $k\to \infty$) and \textbf{infra-red divergence} (i.e. the integral diverges as $k\to 0$) \citep{Naga}.
These divergences we encounter can be explained from physical point of view to be as a result of high energy limits of the momentum integration and infinitely many emitted soft photons.

\textbf{Ultraviolet divergence} is concentrated around the $F_1$ term and therefore it entirely affects the charge of the  electron and not its magnetic moment.
In order to deal with this divergence, we use the charge  \textit{re-normalization} process which enables us to effectively calculate the effects of low energy physics irrespective of its correction at higher energies \citep{Daniel}.
In this procedure, we subtract the value $F_1(0)$ from the first term, which ensures that the remainder is zero at $q^2=0$, and hence the charge remains unaffected \citep{Franz}. Mathematically, at higher orders, we express equation \eqref{3.3.5} as
\[\Gamma^\mu=\big(F_1(q^2)-F_1(0)\big)\gamma^\mu+\frac{i\sigma^{\mu\nu}}{2m}q_\nu F_2(q^2)+F_1(0)\gamma^\mu\]
where the infinite constant $F_1(0)$ becomes absorbed by the multiplicative re-normalization factor
\[Z_1=\frac{1}{F_1(0)+1}.\]
We also note that there are two possible corrections that can take place around a single charge and they include \citep{Franz}:
\begin{enumerate}
\item[$\bullet$] \textit{Proper vertex corrections (one particle irreducible)}-These corrections cannot be divided into two by cutting a propagator or electron line.
\item[$\bullet$]\textit{Improper corrections}-These corrections can be divided into two by a propagator or electron line.
\end{enumerate}
For the calculations in sections \ref{sec.3.3} and \ref{sec.3.4} above, we have considered only the proper corrections but for  higher order correction calculations, the sum of all Feynman diagrams contributing to vertex correction must be considered and expressed in terms of proper vertex.

On the other hand, \textbf{infra-red divergence} cancel 
out with the emissions of very low energy photons, it vanishes to order $\alpha$ if we include virtual photon mass $\beta$ contribution to the vertex \citep{Naga}.

\begin{figure}[h!]
\begin{center}
\begin{tikzpicture}[very thick]
    % draw the two circles and decorate them with arrows
    \draw[draw=purple,
        decoration={markings, mark=at position 0.55 with {\arrow{>}}},
        postaction={decorate}
        ] (0,0) -- (0.8,0.8);
      % \fill (2,2) -- (2,2)
\draw[draw=purple,
        decoration={markings, mark=at position 0.55 with {\arrow{>}}},
        postaction={decorate}
        ] (0.8,1.2) -- (0,2);
\draw[draw=blue, >=stealth, -, snake=snake,segment amplitude = .7mm, segment length = 5mm] (1,1) -- (3,1);
\draw[
        decoration={markings, mark=at position 0.625 with {{-}}},
        postaction={decorate}
        ]
        (1,1) circle (0.3);
\filldraw[even odd rule,inner color=gray,outer color=white] (1,1) circle (0.3);
\node at (3.5,1){$\boldsymbol{=}$};

\draw[draw=purple,
        decoration={markings, mark=at position 0.55 with {\arrow{>}}},
        postaction={decorate}
        ] (4,0) -- (5,1);
\draw[draw=purple,
        decoration={markings, mark=at position 0.55 with {\arrow{>}}},
        postaction={decorate}
        ] (5,1) -- (4,2);
\draw[draw=blue, >=stealth, -, snake=snake,segment amplitude = .7mm, segment length = 5mm] (5,1) -- (6.2,1);
\node at (6.6,1){$\boldsymbol{+}$};
\draw[draw=purple,
        decoration={markings, mark=at position 0.55 with {\arrow{>}}},
        postaction={decorate}
        ] (7,0) -- (8,1);
\draw[draw=purple,
        decoration={markings, mark=at position 0.55 with {\arrow{>}}},
        postaction={decorate}
        ] (8,1) -- (7.1,2);
\draw[draw=blue, >=stealth, -, snake=snake,segment amplitude = .7mm, segment length = 5mm] (8,1) -- (9.2,1);
\draw[draw=blue, >=stealth, -, snake=snake,segment amplitude = .7mm, segment length = 5mm] (7.3,0.3) -- (7.3,1.8);
\node at (9.5,1){$\boldsymbol{+}$};
\draw[draw=purple,
        decoration={markings, mark=at position 0.55 with {\arrow{>}}},
        postaction={decorate}
        ] (10,0) -- (11,1);
\draw[draw=purple,
        decoration={markings, mark=at position 0.55 with {\arrow{>}}},
        postaction={decorate}
        ] (11,1) -- (10,2);
  \draw[
        decoration={markings, mark=at position 0.625 with {\arrow{>}}},
        postaction={decorate}
        ]
        (12.1,1) circle (0.4);  
        
\draw[draw=blue, >=stealth, -, snake=snake,segment amplitude = .7mm, segment length = 5mm] (11,1) -- (11.7,1);
\draw[draw=blue, >=stealth, -, snake=snake,segment amplitude = .7mm, segment length = 5mm] (12.5,1) -- (13.3,1);
\node at (5,-0.5){$\boldsymbol{(1)}$};
\node at (7.8,-0.5){$\boldsymbol{(2)}$};
\node at (11,-0.5){$\boldsymbol{(3)}$};
\end{tikzpicture}
%\caption{ Diagrams of higher order correction to the vertex in QED}
\end{center}
\end{figure}

\begin{figure}[h!]
\begin{center}
\begin{tikzpicture}[very thick]
\node at (3,1){$\boldsymbol{+}$};
\draw[draw=purple,
        decoration={markings, mark=at position 0.55 with {\arrow{>}}},
        postaction={decorate}
        ] (4,0) -- (5,1);
\draw[draw=purple,
        decoration={markings, mark=at position 0.55 with {\arrow{>}}},
        postaction={decorate}
        ] (5,1) -- (4,2);
\draw[draw=blue, >=stealth, -, snake=snake,segment amplitude = .7mm, segment length = 5mm] (5,1) -- (6.8,1);
\node at (7.4,1){$\boldsymbol{+}$};
\draw[draw=purple,
        decoration={markings, mark=at position 0.55 with {\arrow{>}}},
        postaction={decorate}
        ] (8,0) -- (9,1);
\draw[draw=purple,
        decoration={markings, mark=at position 0.55 with {\arrow{>}}},
        postaction={decorate}
        ] (9,1) -- (8,2);
\draw[draw=blue, >=stealth, -, snake=snake,segment amplitude = .7mm, segment length = 5mm] (9,1) -- (11.2,1);
\draw[draw=blue, >=stealth, -, snake=snake,segment amplitude = .7mm, segment length = 5mm] (8.3,0.3) -- (8.1,1);
\draw[draw=blue, >=stealth, -, snake=snake,segment amplitude = .7mm, segment length = 5mm] (8.3,1.7) -- (8.1,2.4);
\node at (5,-0.5){$\boldsymbol{(4)}$};
\node at (8.5,-0.5){$\boldsymbol{(5)}$};
\draw[draw=blue, decorate, decoration ={snake}](4.3,0.3)arc(-10:-80:-0.7);
\draw[draw=blue, decorate, decoration ={snake}](4.2,1.8)arc(5:93:-0.62);
\node at (12.5,1){e.t.c.};
\end{tikzpicture}
\caption{ Diagrams of higher order correction to the vertex in QED.}
\end{center}
\end{figure}
From the figures above: 
\begin{enumerate}
\item[$\bullet$](1)- represents a bare vertex without any loop hence no correction.
\item[$\bullet$](2)- represents the vertex correction we have used in our calculations.
\item[$\bullet$](3)- represents the photon propagator correction.
\item[$\bullet$](4)- represents the fermion propagator correction.
\item[$\bullet$](5)- represents the correction as a result of emission of very low energy photons.
\end{enumerate}










